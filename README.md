# transaction-importer project

This project uses Reactive Spring Framework.

If you want to learn more about Reactive Spring, please visit: https://spring.io/reactive .

## Model

```plantuml

class InputType {
  + String name
  + String importer
}


class TransactionInput {
  + String content
  + InputType type
  + InputStatus status
  + Long accountId
}
TransactionInput "*" *--o "1" InputType : type

```

[//]: # "mastic-pin-readme"
