package io.gitlab.getoutlay.db;

import static io.gitlab.getoutlay.db.InputTypePopulator.SPARKASSE;
import static io.gitlab.getoutlay.db.InputTypePopulator.SPARKASSE_BEAN;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import io.gitlab.getoutlay.InputType;
import io.gitlab.getoutlay.InputTypeRepository;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@DirtiesContext(classMode = ClassMode.BEFORE_CLASS)
@ActiveProfiles("test")
class InputTypePopulatorTest {

  @Autowired
  InputTypeRepository inputTypeRepository;

  @Test
  public void isCreated() {
    assertNotNull(inputTypeRepository);
  }

  @Test
  public void hasImporters() {
    List<InputType> list = inputTypeRepository.findAll()
        .collectList()
        .block();
    assertNotNull(list);
    assertFalse(list.isEmpty());
  }

  @Test
  public void hasSparkasseImporter() {
    InputType inputType = inputTypeRepository.findByName(SPARKASSE)
        .block();
    assertNotNull(inputType);
    assertEquals(SPARKASSE, inputType.getName());
    assertEquals(SPARKASSE_BEAN, inputType.getImporter());
  }

}