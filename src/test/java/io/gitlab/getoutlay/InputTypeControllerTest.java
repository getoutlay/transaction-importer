package io.gitlab.getoutlay;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.in;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.Objects;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;

@ControllerTest
public class InputTypeControllerTest {

  @Autowired
  ApplicationContext context;

  @Autowired
  TestData data;

  @Autowired
  InputTypeRepository repository;

  WebTestClient rest;

  @BeforeEach
  public void setUp() {
    rest = WebTestClient.bindToApplicationContext(context)
        .configureClient()
        .responseTimeout(Duration.ofHours(1L))
        .build();
  }

  @Test
  public void createGetAndList() {

    InputType entity = new InputType();
    entity.setName("Malayan Tapir");
    entity.setImporter("Malayan Tapir");

    InputType created = rest.post()
        .uri("/input-types")
        .accept(MediaType.APPLICATION_JSON)
        .bodyValue(entity)
        .exchange()
        .expectStatus()
        .isCreated()
        .expectBody(InputType.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(created);
    assertEquals("Malayan Tapir", created.getName());
    assertEquals("Malayan Tapir", created.getImporter());

    InputType single = rest.get()
        .uri("/input-types/{id}", created.getId())
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(InputType.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(single);
    assertEquals(created.getId(), single.getId());

    List<InputType> list = rest.get()
        .uri("/input-types")
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(new ParameterizedTypeReference<List<InputType>>() {
        })
        .returnResult()
        .getResponseBody();

    assertNotNull(list);
    assertThat(list, not(empty()));
    assertThat(created, is(in(list)));
  }

  @Test
  public void createUpdateAndGet() {

    InputType entity = new InputType();
    entity.setName("Malayan Tapir");
    entity.setImporter("Malayan Tapir");

    InputType created = rest.post()
        .uri("/input-types")
        .accept(MediaType.APPLICATION_JSON)
        .bodyValue(entity)
        .exchange()
        .expectStatus()
        .isCreated()
        .expectBody(InputType.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(created);
    assertEquals("Malayan Tapir", created.getName());
    assertEquals("Malayan Tapir", created.getImporter());

    entity.setName("Malayan Tapir Updated");
    entity.setImporter("Malayan Tapir Updated");

    InputType updated = rest.put()
        .uri("/input-types/{id}", created.getId())
        .accept(MediaType.APPLICATION_JSON)
        .bodyValue(entity)
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(InputType.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(updated);
    assertEquals("Malayan Tapir Updated", updated.getName());
    assertEquals("Malayan Tapir Updated", updated.getImporter());

    InputType single = rest.get()
        .uri("/input-types/{id}", created.getId())
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(InputType.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(single);
    assertEquals(created.getId(), single.getId());
  }

  @Test
  public void createDeleteAndGet() {

    InputType entity = new InputType();
    entity.setName("Malayan Tapir");
    entity.setImporter("Malayan Tapir");

    InputType created = rest.post()
        .uri("/input-types")
        .accept(MediaType.APPLICATION_JSON)
        .bodyValue(entity)
        .exchange()
        .expectStatus()
        .isCreated()
        .expectBody(InputType.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(created);
    assertEquals("Malayan Tapir", created.getName());
    assertEquals("Malayan Tapir", created.getImporter());

    InputType deleted = rest.delete()
        .uri("/input-types/{id}", created.getId())
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(InputType.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(deleted);
    assertEquals(created.getId(), deleted.getId());

    rest.get()
        .uri("/input-types/{id}", created.getId())
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isNotFound();
  }

  @Test
  public void getInvalid() {
    rest.get()
        .uri("/input-types/{id}", "invalid")
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isBadRequest();
  }

  @Test
  public void getMissing() {
    rest.get()
        .uri("/input-types/{id}", -666L)
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isNotFound();
  }
  
  
}
