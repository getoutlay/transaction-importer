package io.gitlab.getoutlay.batch;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import io.gitlab.getoutlay.InputStatus;
import io.gitlab.getoutlay.TransactionInput;
import io.gitlab.getoutlay.TransactionInputRepository;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.Sort;
import reactor.core.publisher.Flux;

class NextTransactionInputReaderTest {

  @Test
  void transactionInputStatusSetToRunningAndSaved() {
    TransactionInputRepository transactionInputRepository = spy(TransactionInputRepository.class);
    TransactionInput transactionInput = new TransactionInput();
    transactionInput.setContent("abc");
    transactionInput.setTypeId(1L);
    transactionInput.setStatus(InputStatus.INITIALIZED);
    when(transactionInputRepository.findByStatus(eq(InputStatus.INITIALIZED), any(Sort.class)))
        .thenReturn(Flux.just(transactionInput));

    NextTransactionInputReader reader = new NextTransactionInputReader();
    reader.transactionInputRepository = transactionInputRepository;
    TransactionInput ti = reader.read();

    assertNotNull(ti);
    assertEquals(ti, transactionInput);
  }

}
