package io.gitlab.getoutlay.batch;

import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import io.gitlab.getoutlay.InputStatus;
import io.gitlab.getoutlay.TransactionInput;
import io.gitlab.getoutlay.TransactionInputRepository;
import io.gitlab.getoutlay.vault.client.RestClient;
import io.gitlab.getoutlay.vault.client.Transaction;
import io.gitlab.getoutlay.vault.client.VaultClient;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Mono;

@ExtendWith(MockitoExtension.class)
class TransactionInputWriterTest {

  @Mock
  TransactionInputRepository transactionInputRepository;

  @Mock
  VaultClient vaultClient;

  @Mock
  RestClient<Transaction> transactionClient;

  @Captor
  ArgumentCaptor<TransactionInput> transactionInputArgument;

  TransactionInputWriter writer;

  @BeforeEach
  void init() {
    writer = new TransactionInputWriter();
    writer.transactionInputRepository = transactionInputRepository;
    writer.vaultClient = vaultClient;

    lenient().when(vaultClient.transaction()).thenReturn(transactionClient);
    lenient().when(transactionClient.create(any(Transaction.class)))
        .thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
    lenient().when(transactionInputRepository.save(any(TransactionInput.class)))
        .thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
  }

  @Test
  void nullParameterThrowsException() {
    //noinspection ConstantConditions
    Assertions.assertThrows(IllegalArgumentException.class, () -> writer.write(null));
  }

  @Test
  void nullElementsInParameterThrowsException() {
    List<TransactionInputEntry> nullElements = singletonList(null);
    Assertions.assertThrows(IllegalArgumentException.class, () -> writer.write(nullElements));
  }

  @Test
  void entryWithNullTransactionInputThrowsException() {
    List<TransactionInputEntry> nullElements = singletonList(new TransactionInputEntry(null, Collections.emptyList()));
    Assertions.assertThrows(IllegalArgumentException.class, () -> writer.write(nullElements));
  }


  @Test
  void writesWithoutTransactions() {
    TransactionInput transactionInput = new TransactionInput();
    transactionInput.setStatus(InputStatus.RUNNING);
    transactionInput.setContent("test");
    transactionInput.setAccountId(42L);
    transactionInput.setTypeId(42L);

    List<TransactionInputEntry> inputEntries = singletonList(new TransactionInputEntry(transactionInput, Collections.emptyList()));
    writer.write(inputEntries);

    verify(transactionInputRepository).save(transactionInputArgument.capture());
    assertEquals(InputStatus.SUCCESS, transactionInputArgument.getValue().getStatus());
    verify(transactionClient, never()).create(any(Transaction.class));
  }

  @Test
  void writesWithTransactions() {
    TransactionInput transactionInput = new TransactionInput();
    transactionInput.setStatus(InputStatus.RUNNING);
    transactionInput.setContent("test");
    transactionInput.setAccountId(42L);
    transactionInput.setTypeId(42L);

    List<Transaction> transactions = Arrays.asList(new Transaction(), new Transaction(), new Transaction());
    List<TransactionInputEntry> inputEntries = singletonList(new TransactionInputEntry(transactionInput,
        transactions));
    writer.write(inputEntries);

    verify(transactionInputRepository).save(transactionInputArgument.capture());
    assertEquals(InputStatus.SUCCESS, transactionInputArgument.getValue().getStatus());
    verify(transactionClient, times(transactions.size())).create(any(Transaction.class));
  }

  @Test
  void vaultClientErrorDuringWriting() {
    lenient().when(transactionClient.create(any(Transaction.class)))
        .thenReturn(Mono.error(new RuntimeException("Test error")));

    TransactionInput transactionInput = new TransactionInput();
    transactionInput.setStatus(InputStatus.RUNNING);
    transactionInput.setContent("test");
    transactionInput.setAccountId(42L);
    transactionInput.setTypeId(42L);
    Transaction transaction = new Transaction();

    List<TransactionInputEntry> inputEntries = singletonList(new TransactionInputEntry(transactionInput, singletonList(transaction)));
    writer.write(inputEntries);

    verify(transactionInputRepository).save(transactionInputArgument.capture());
    assertEquals(InputStatus.FAILED, transactionInputArgument.getValue().getStatus());
    verify(transactionClient, only()).create(eq(transaction));
  }
}