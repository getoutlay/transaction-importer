package io.gitlab.getoutlay.batch;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyListOf;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import io.gitlab.getoutlay.TransactionInput;
import java.util.Collections;
import java.util.List;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.batch.test.context.SpringBatchTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.task.SyncTaskExecutor;
import org.springframework.test.context.ActiveProfiles;

@SpringBatchTest
@SpringBootTest
@ActiveProfiles("test")
class BatchConfigurationTest {

  @Autowired
  JobLauncherTestUtils testUtils;

  @MockBean
  NextTransactionInputReader nextTransactionInputReader;

  @MockBean
  TransactionInputProcessor transactionInputProcessor;

  @MockBean
  TransactionInputWriter transactionInputWriter;

  @Test
  public void testImportTransactionJobWithEmptyInput() throws Exception {
    when(nextTransactionInputReader.read()).thenReturn(null);
    JobExecution jobExecution = testUtils.launchJob();

    assertNotNull(jobExecution);
    while(jobExecution.getStatus() == BatchStatus.STARTING || jobExecution.isRunning()) {
      Thread.sleep(100);
    }
    assertEquals(ExitStatus.COMPLETED, jobExecution.getExitStatus());
  }

  @Test
  public void testImportTransactionJobWithInput() throws Exception {
    TransactionInput transactionInput = new TransactionInput();
    TransactionInputEntry inputEntry = new TransactionInputEntry(transactionInput, Collections.emptyList());
    when(transactionInputProcessor.process(eq(transactionInput)))
        .thenReturn(inputEntry);

    transactionInput.setContent("test-content");
    TransactionInput[] nextTransactionInput = new TransactionInput[]{null};
    when(nextTransactionInputReader.read()).thenReturn(transactionInput, nextTransactionInput);
    JobExecution jobExecution = testUtils.launchJob();

    assertNotNull(jobExecution);
    while(jobExecution.getStatus() == BatchStatus.STARTING || jobExecution.isRunning()) {
      Thread.sleep(100);
    }
    assertEquals(ExitStatus.COMPLETED, jobExecution.getExitStatus());
  }
}
