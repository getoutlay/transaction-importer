package io.gitlab.getoutlay.batch;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import io.gitlab.getoutlay.InputStatus;
import io.gitlab.getoutlay.InputType;
import io.gitlab.getoutlay.InputTypeRepository;
import io.gitlab.getoutlay.TransactionInput;
import io.gitlab.getoutlay.TransactionInputRepository;
import io.gitlab.getoutlay.reader.TransactionReader;
import io.gitlab.getoutlay.reader.TransactionReaderLoader;
import io.gitlab.getoutlay.vault.client.Transaction;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Mono;

@ExtendWith(MockitoExtension.class)
class TransactionInputProcessorTest {

  @Spy
  TransactionInputRepository transactionInputRepository;

  @Spy
  InputTypeRepository inputTypeRepository;

  @Spy
  TransactionReaderLoader transactionReaderLoader;

  @Captor
  ArgumentCaptor<TransactionInput> transactionInputArgument;

  TransactionInputProcessor processor;

  @BeforeEach
  void initProcessor() {
    processor = new TransactionInputProcessor();
    processor.transactionInputRepository = transactionInputRepository;
    processor.inputTypeRepository = inputTypeRepository;
    processor.transactionReaderLoader = transactionReaderLoader;
  }

  @Test
  void nullTransactionInputThrowsException() {
    //noinspection ConstantConditions
    Assertions.assertThrows(IllegalArgumentException.class, () -> processor.process(null));
  }

  @Test
  void emptyTransactionInputThrowsException() {
    Assertions.assertThrows(NullPointerException.class, () -> processor.process(new TransactionInput()));
  }


  @Test
  void missingImporterLeadsToFailedInputStatus() {
    InputType inputType = new InputType();
    when(transactionInputRepository.save(any(TransactionInput.class)))
        .thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
    when(inputTypeRepository.findById(eq(1L)))
        .thenReturn(Mono.just(inputType));
    when(transactionReaderLoader.loadReaderFor(eq(inputType)))
        .thenReturn(Optional.empty());

    TransactionInput transactionInput = new TransactionInput();
    transactionInput.setContent("abc");
    transactionInput.setTypeId(1L);
    transactionInput.setStatus(InputStatus.INITIALIZED);
    TransactionInputEntry entry = processor.process(transactionInput);

    assertNull(entry);
    verify(transactionInputRepository, times(2)).save(transactionInputArgument.capture());
    assertEquals(InputStatus.FAILED, transactionInputArgument.getValue().getStatus());
  }

  @Test
  void missingInputTypeLeadsToFailedInputStatus() {
    when(transactionInputRepository.save(any(TransactionInput.class)))
        .thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
    when(inputTypeRepository.findById(eq(1L)))
        .thenReturn(Mono.empty());

    TransactionInput transactionInput = new TransactionInput();
    transactionInput.setContent("abc");
    transactionInput.setTypeId(1L);
    transactionInput.setStatus(InputStatus.INITIALIZED);
    TransactionInputEntry entry = processor.process(transactionInput);

    assertNull(entry);
    verify(transactionInputRepository, times(2)).save(transactionInputArgument.capture());
    assertEquals(InputStatus.FAILED, transactionInputArgument.getValue().getStatus());
  }

  @Test
  void transactionInputStatusSetToRunningAndSaved() {
    InputType inputType = new InputType();
    List<Transaction> transactions = Arrays.asList(new Transaction(), new Transaction());
    when(transactionInputRepository.save(any(TransactionInput.class)))
        .thenAnswer(invocation -> Mono.just(invocation.getArgument(0)));
    when(inputTypeRepository.findById(eq(1L)))
        .thenReturn(Mono.just(inputType));
    when(transactionReaderLoader.loadReaderFor(eq(inputType)))
        .thenReturn(Optional.of(new TransactionReader() {
          @Override
          protected List<Transaction> read(String content) {
            return transactions;
          }
        }));

    TransactionInput transactionInput = new TransactionInput();
    transactionInput.setContent("abc");
    transactionInput.setTypeId(1L);
    transactionInput.setStatus(InputStatus.INITIALIZED);
    TransactionInputEntry entry = processor.process(transactionInput);

    assertNotNull(entry);
    assertNotNull(entry.getTransactionInput());
    assertEquals(InputStatus.RUNNING, entry.getTransactionInput().getStatus());
    assertEquals(transactions, entry.getTransactions());
    verify(transactionInputRepository).save(transactionInput);
  }

}
