package io.gitlab.getoutlay.util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class Base64UtilsTest {

  @Test
  void decodeBase64WithHeader() {
    assertEquals("deneme;test\n", Base64Utils.decodeBase64("data:text/csv;base64,ZGVuZW1lO3Rlc3QK"));
  }

  @Test
  void decodeBase64WithoutHeader() {
    assertEquals("deneme;test\n", Base64Utils.decodeBase64("ZGVuZW1lO3Rlc3QK"));
  }
}