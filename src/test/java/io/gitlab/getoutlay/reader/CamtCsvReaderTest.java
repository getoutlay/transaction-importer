package io.gitlab.getoutlay.reader;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import io.gitlab.getoutlay.TransactionInput;
import io.gitlab.getoutlay.vault.client.Transaction;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.FileCopyUtils;

class CamtCsvReaderTest {

  CamtCsvReader reader = new CamtCsvReader();

  @Test
  void readEmptyReturnsNull() {
    List<Transaction> transactions = reader.read("");
    assertNull(transactions);
  }

  @Test
  void readNullContentReturnsNull() {
    List<Transaction> transactions = reader.read((String) null);
    assertNull(transactions);
  }

  @Test
  void readNullTransactionInputReturnsNull() {
    List<Transaction> transactions = reader.read((TransactionInput) null);
    assertNull(transactions);
  }

  @Test
  void readNullContentOfTransactionInputReturnsNull() {
    List<Transaction> transactions = reader.read(new TransactionInput());
    assertNull(transactions);
  }

  @Test
  void readInvalidContentReturnsEmptyList() {
    List<Transaction> transactions = reader.read("i;n;v;a;l;i;d content!?");
    assertNotNull(transactions);
    assertTrue(transactions.isEmpty());
  }

  @Test
  void readSuccess() throws IOException {
    ClassLoader classLoader = getClass().getClassLoader();
    ClassPathResource resource = new ClassPathResource("test-data/camt-transactions.csv", classLoader);
    InputStreamReader inputStreamReader = new InputStreamReader(resource.getInputStream(), UTF_8);
    String content = FileCopyUtils.copyToString(inputStreamReader);
    List<Transaction> transactions = reader.read(content);
    assertNotNull(transactions);
    assertEquals(3, transactions.size());

    assertEquals(-10d, transactions.get(0).getAmount(), .001);
    assertEquals(-38.67d, transactions.get(1).getAmount(), .001);
    assertEquals(-276.34d, transactions.get(2).getAmount(), .001);
  }

}