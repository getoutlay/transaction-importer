package io.gitlab.getoutlay.reader;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import io.gitlab.getoutlay.InputType;
import io.gitlab.getoutlay.db.InputTypePopulator;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("test")
public class TransactionReaderBeansTest {

  @Autowired
  TransactionReaderLoader transactionReaderLoader;

  @Test
  void importerLoaderInstance() {
    assertNotNull(transactionReaderLoader);
  }

  @Test
  void sparkasseImporterLoaded() {
    InputType inputType = new InputType();
    inputType.setImporter(InputTypePopulator.SPARKASSE_BEAN);
    Optional<TransactionReader> sparkasseImporter = transactionReaderLoader.loadReaderFor(inputType);

    assertNotNull(sparkasseImporter);
    assertFalse(sparkasseImporter.isEmpty());
    assertNotNull(sparkasseImporter.get());
    assertEquals(CamtCsvReader.class, sparkasseImporter.get().getClass());
  }

}
