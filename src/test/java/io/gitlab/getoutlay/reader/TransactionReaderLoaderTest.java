package io.gitlab.getoutlay.reader;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import io.gitlab.getoutlay.InputType;
import io.gitlab.getoutlay.vault.client.Transaction;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.ApplicationContext;

@ExtendWith(MockitoExtension.class)
class TransactionReaderLoaderTest {

  @Mock
  ApplicationContext applicationContext;

  TransactionReaderLoader transactionReaderLoader;

  @BeforeEach
  void initLoader() {
    transactionReaderLoader = new TransactionReaderLoader();
    transactionReaderLoader.applicationContext = applicationContext;
  }

  @Test
  void cannotLoadImporterForNullInputType() {
    Optional<TransactionReader> importer = transactionReaderLoader.loadReaderFor(null);
    assertNotNull(importer);
    assertTrue(importer.isEmpty());
  }

  @Test
  void cannotLoadImporterForNullImporterClassName1() {
    Optional<TransactionReader> importer = transactionReaderLoader.loadReaderFor(new InputType());
    assertNotNull(importer);
    assertTrue(importer.isEmpty());
  }

  @Test
  void cannotLoadImporterForEmptyImporterClassName() {
    InputType inputType = new InputType();
    inputType.setImporter("");
    Optional<TransactionReader> importer = transactionReaderLoader.loadReaderFor(inputType);
    assertNotNull(importer);
    assertTrue(importer.isEmpty());
  }

  @Test
  void cannotLoadImporterForMissingBean() {
    when(applicationContext.getBean(eq("someBean")))
        .thenThrow(new NoSuchBeanDefinitionException("someBean"));

    InputType inputType = new InputType();
    inputType.setImporter("someBean");

    Optional<TransactionReader> importer = transactionReaderLoader.loadReaderFor(inputType);
    assertNotNull(importer);
    assertTrue(importer.isEmpty());
  }

  static class TestBean {

  }

  @Test
  void cannotLoadImporterIfBeanIsNotImporterType() {
    when(applicationContext.getBean(eq("someBean")))
        .thenReturn(new TestBean());

    InputType inputType = new InputType();
    inputType.setImporter("someBean");

    Optional<TransactionReader> importer = transactionReaderLoader.loadReaderFor(inputType);
    assertNotNull(importer);
    assertTrue(importer.isEmpty());
  }

  static class TestTransactionReader extends TransactionReader {

    @Override
    protected List<Transaction> read(String content) {
      return Collections.emptyList();
    }
  }

  @Test
  void loadImporterSuccess() {
    when(applicationContext.getBean(eq("testImporter")))
        .thenReturn(new TestTransactionReader());

    InputType inputType = new InputType();
    inputType.setImporter("testImporter");

    Optional<TransactionReader> importer = transactionReaderLoader.loadReaderFor(inputType);
    assertNotNull(importer);
    assertFalse(importer.isEmpty());
    assertNotNull(importer.get());
    assertEquals(TestTransactionReader.class, importer.get().getClass());
  }

}