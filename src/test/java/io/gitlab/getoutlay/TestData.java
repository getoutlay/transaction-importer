package io.gitlab.getoutlay;

import org.springframework.stereotype.Component;

@Component
public class TestData {

  private InputType inputType;
  private TransactionInput transactionInput;

  public InputType getInputType() {
    return inputType;
  }

  public TransactionInput getTransactionInput() {
    return transactionInput;
  }


  public void setInputType(InputType inputType) {
    this.inputType = inputType;
  }

  public void setTransactionInput(TransactionInput transactionInput) {
    this.transactionInput = transactionInput;
  }


  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("TestData {\n");
    sb.append(" inputType=").append(inputType).append("\n");
    sb.append(" transactionInput=").append(transactionInput).append("\n");
    sb.append('}');
    return sb.toString();
  }

}
