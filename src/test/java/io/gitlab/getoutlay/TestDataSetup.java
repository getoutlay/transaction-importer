package io.gitlab.getoutlay;

import static io.gitlab.getoutlay.db.InputTypePopulator.CAMT_CSV_READER;
import static io.gitlab.getoutlay.db.InputTypePopulator.CAMT_CSV_READER_BEAN;

import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

public class TestDataSetup implements BeforeAllCallback {

  private final Logger logger = LoggerFactory.getLogger(getClass());

  private final Object lock = new Object();
  private static boolean initialized = false;

  @Override
  public void beforeAll(ExtensionContext context) {
    synchronized (lock) {
      if (!initialized) {
        initialized = true;
        logger.info("initializing test data...");

        final ApplicationContext ctx = SpringExtension.getApplicationContext(context);
        final TestData data = ctx.getBean(TestData.class);
        logger.debug("initial test data: {}", data);

        final InputTypeRepository inputTypeRepository = ctx.getBean(InputTypeRepository.class);
        logger.debug("creating InputType...");
        final InputType inputType = new InputType();
        inputType.setName(CAMT_CSV_READER);
        inputType.setImporter(CAMT_CSV_READER_BEAN);
        data.setInputType(inputTypeRepository.deleteAll()
            .then(inputTypeRepository.save(inputType))
            .block());
        logger.info("test InputType: {}", data.getInputType());

        final TransactionInputRepository transactionInputRepository = ctx.getBean(TransactionInputRepository.class);
        logger.debug("creating TransactionInput...");
        final TransactionInput transactionInput = new TransactionInput();
        transactionInput.setContent("Malayan Tapir");
        transactionInput.setTypeId(data.getInputType().getId());
        transactionInput.setStatus(InputStatus.values()[0]);
        transactionInput.setAccountId(17983L);
        data.setTransactionInput(transactionInputRepository.deleteAll()
            .then(transactionInputRepository.save(transactionInput))
            .block());
        logger.info("test TransactionInput: {}", data.getTransactionInput());

        logger.info("test data initialized: {}", data);
      }
    }
  }

}
