package io.gitlab.getoutlay;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.in;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.time.Duration;
import java.util.List;
import java.util.Objects;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;

@ControllerTest
public class TransactionInputControllerTest {

  @Autowired
  ApplicationContext context;

  @Autowired
  TestData data;

  @Autowired
  TransactionInputRepository repository;

  WebTestClient rest;

  @BeforeEach
  public void setUp() {
    rest = WebTestClient.bindToApplicationContext(context)
        .configureClient()
        .responseTimeout(Duration.ofHours(1L))
        .build();
  }

  @Test
  public void createGetAndList() {

    TransactionInput entity = new TransactionInput();
    entity.setContent("TWFsYXlhbiBUYXBpcg==");
    entity.setTypeId(data.getInputType().getId());
    entity.setStatus(InputStatus.values()[0]);
    entity.setAccountId(17983L);

    TransactionInput created = rest.post()
        .uri("/transaction-inputs")
        .accept(MediaType.APPLICATION_JSON)
        .bodyValue(entity)
        .exchange()
        .expectStatus()
        .isCreated()
        .expectBody(TransactionInput.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(created);
    assertEquals("Malayan Tapir", created.getContent());
    assertEquals(data.getInputType().getId(), created.getTypeId());
    assertEquals(InputStatus.values()[0], created.getStatus());
    assertEquals(17983L, created.getAccountId());

    TransactionInput single = rest.get()
        .uri("/transaction-inputs/{id}", created.getId())
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(TransactionInput.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(single);
    assertEquals(created.getId(), single.getId());

    List<TransactionInput> list = rest.get()
        .uri("/transaction-inputs")
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(new ParameterizedTypeReference<List<TransactionInput>>() {
        })
        .returnResult()
        .getResponseBody();

    assertNotNull(list);
    assertThat(list, not(empty()));
    assertThat(created, is(in(list)));
  }

  @Test
  public void createUpdateAndGet() {

    TransactionInputCreateDao entity = new TransactionInputCreateDao();
    entity.setContent("TWFsYXlhbiBUYXBpcg==");
    entity.setAccountId(17983L);

    TransactionInput created = rest.post()
        .uri("/transaction-inputs")
        .accept(MediaType.APPLICATION_JSON)
        .bodyValue(entity)
        .exchange()
        .expectStatus()
        .isCreated()
        .expectBody(TransactionInput.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(created);
    assertEquals("Malayan Tapir", created.getContent());
    assertEquals(17983L, created.getAccountId());

    entity.setContent("TWFsYXlhbiBUYXBpciBVcGRhdGVk");
    entity.setAccountId(18025L);

    TransactionInput updated = rest.put()
        .uri("/transaction-inputs/{id}", created.getId())
        .accept(MediaType.APPLICATION_JSON)
        .bodyValue(entity)
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(TransactionInput.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(updated);
    assertEquals("Malayan Tapir Updated", updated.getContent());
    assertEquals(data.getInputType().getId(), updated.getTypeId());
    assertEquals(InputStatus.values()[0], updated.getStatus());
    assertEquals(18025L, updated.getAccountId());

    TransactionInput single = rest.get()
        .uri("/transaction-inputs/{id}", created.getId())
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(TransactionInput.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(single);
    assertEquals(created.getId(), single.getId());
  }

  @Test
  public void createDeleteAndGet() {

    TransactionInput entity = new TransactionInput();
    entity.setContent("TWFsYXlhbiBUYXBpcg==");
    entity.setTypeId(data.getInputType().getId());
    entity.setStatus(InputStatus.values()[0]);
    entity.setAccountId(17983L);

    TransactionInput created = rest.post()
        .uri("/transaction-inputs")
        .accept(MediaType.APPLICATION_JSON)
        .bodyValue(entity)
        .exchange()
        .expectStatus()
        .isCreated()
        .expectBody(TransactionInput.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(created);
    assertEquals("Malayan Tapir", created.getContent());
    assertEquals(data.getInputType().getId(), created.getTypeId());
    assertEquals(InputStatus.values()[0], created.getStatus());
    assertEquals(17983L, created.getAccountId());

    TransactionInput deleted = rest.delete()
        .uri("/transaction-inputs/{id}", created.getId())
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(TransactionInput.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(deleted);
    assertEquals(created.getId(), deleted.getId());

    rest.get()
        .uri("/transaction-inputs/{id}", created.getId())
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isNotFound();
  }

  @Test
  public void getInvalid() {
    rest.get()
        .uri("/transaction-inputs/{id}", "invalid")
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isBadRequest();
  }

  @Test
  public void getMissing() {
    rest.get()
        .uri("/transaction-inputs/{id}", -666L)
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isNotFound();
  }

  @Test
  public void getType() {
    TransactionInput entity = new TransactionInput();
    entity.setContent("Malayan Tapir");
    entity.setTypeId(data.getInputType().getId());
    entity.setStatus(InputStatus.values()[0]);
    entity.setAccountId(17983L);

    TransactionInput transactionInput = Objects.requireNonNull(repository.save(entity).block());

    InputType result = rest.get()
        .uri("/transaction-inputs/{id}/type", transactionInput.getId())
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(InputType.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(result);
    assertEquals(result.getId(), result.getId());
  }

  @Test
  public void getInvalidType() {
    rest.get()
        .uri("/transaction-inputs/{id}/type", "invalid")
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isBadRequest();
  }

  @Test
  public void getMissingType() {
    rest.get()
        .uri("/transaction-inputs/{id}/type", -666L)
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isNotFound();
  }


}
