package io.gitlab.getoutlay;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;
import javax.validation.constraints.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;


@Table("T_INPUT_TYPE")
public class InputType {

  @Id
  private Long id;
  
  @NotNull
  @Column("NAME")
  @JsonProperty
  private String name;

  @NotNull
  @Column("IMPORTER")
  @JsonProperty
  private String importer;

  public Long getId() {
    return id;
  }

  
  public String getName() { return name; }

  public String getImporter() { return importer; }

  public void setId(Long id) {
    this.id = id;
  }

  
  public void setName(String name) { this.name = name; }

  public void setImporter(String importer) { this.importer = importer; }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    InputType inputType = (InputType) o;
    return Objects.equals(id, inputType.id)
        && Objects.equals(name, inputType.name)
        && Objects.equals(importer, inputType.importer);
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("InputType{");
    sb.append("id=").append(id);
    sb.append(", name=").append(name);
    sb.append(", importer=").append(importer);
    sb.append('}');
    return sb.toString();
  }

}
