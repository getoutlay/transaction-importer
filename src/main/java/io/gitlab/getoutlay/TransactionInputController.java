package io.gitlab.getoutlay;

import static io.gitlab.getoutlay.db.InputTypePopulator.CAMT_CSV_READER;
import static io.gitlab.getoutlay.util.Base64Utils.decodeBase64;
import static io.gitlab.getoutlay.util.FluxUtils.wrapNotFoundIfEmpty;

import io.gitlab.getoutlay.batch.BatchTransactionInputService;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/transaction-inputs")
public class TransactionInputController {

  private final Logger logger = LoggerFactory.getLogger(getClass());
  private final TransactionInputRepository repository;
  private final InputTypeRepository typeRepository;
  private final BatchTransactionInputService batchTransactionInputService;

  public TransactionInputController(TransactionInputRepository repository, InputTypeRepository typeRepository,
      BatchTransactionInputService batchTransactionInputService) {
    this.repository = repository;
    this.typeRepository = typeRepository;
    this.batchTransactionInputService = batchTransactionInputService;
  }

  @GetMapping()
  public Flux<TransactionInput> listAll() {
    logger.debug("listAll()");
    return repository.findAll();
  }

  @GetMapping("/{id}")
  public Mono<TransactionInput> getOne(@PathVariable Long id) {
    logger.debug("getOne({})", id);
    return wrapNotFoundIfEmpty(repository.findById(id), "transactionInput", id);
  }

  @DeleteMapping("/{id}")
  public Mono<TransactionInput> delete(@PathVariable Long id) {
    logger.debug("delete({})", id);
    return wrapNotFoundIfEmpty(repository.findById(id)
        .flatMap(transactionInput -> repository.delete(transactionInput).thenReturn(transactionInput)), "transactionInput", id);
  }

  @PostMapping()
  @ResponseStatus(HttpStatus.CREATED)
  public Mono<TransactionInput> create(@RequestBody @Valid TransactionInputCreateDao dao) {
    logger.debug("create({})", dao);
    return typeRepository.findByName(CAMT_CSV_READER)
        .flatMap(inputType -> {
          TransactionInput transactionInput = new TransactionInput();
          String base64Content = dao.getContent();
          String content = decodeBase64(base64Content);
          transactionInput.setContent(content);
          transactionInput.setStatus(InputStatus.INITIALIZED);
          transactionInput.setTypeId(inputType.getId());
          transactionInput.setAccountId(dao.getAccountId());
          return repository.save(transactionInput)
              .map(ti -> {
                batchTransactionInputService.importTransactions(ti);
                return ti;
              });
        });
  }

  @PutMapping("/{id}")
  public Mono<TransactionInput> update(@PathVariable Long id, @RequestBody @Valid TransactionInputUpdateDao dao) {
    logger.debug("update({} : {})", id, dao);
    return typeRepository.findByName(CAMT_CSV_READER)
        .flatMap(inputType -> {
          TransactionInput transactionInput = new TransactionInput();
          transactionInput.setId(id);
          String base64Content = dao.getContent();
          String content = decodeBase64(base64Content);
          transactionInput.setContent(content);
          transactionInput.setStatus(InputStatus.INITIALIZED);
          transactionInput.setTypeId(inputType.getId());
          transactionInput.setAccountId(dao.getAccountId());
          return wrapNotFoundIfEmpty(repository.save(transactionInput).map(ti -> {
            batchTransactionInputService.importTransactions(ti);
            return ti;
          }), "transactionInput", id);
        });
  }

  @GetMapping("/{id}/type")
  public Mono<InputType> getType(@PathVariable Long id) {
    logger.debug("getType({})", id);
    return wrapNotFoundIfEmpty(repository.findById(id), "order", id)
        .flatMap(order -> {
          Long typeId = order.getTypeId();
          return wrapNotFoundIfEmpty(typeRepository.findById(typeId), "type", typeId);
        });
  }


}
