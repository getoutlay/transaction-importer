package io.gitlab.getoutlay;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;


@Repository
public interface InputTypeRepository extends ReactiveCrudRepository<InputType, Long> {

  Mono<InputType> findByName(String name);

}
