package io.gitlab.getoutlay.db;

import org.springframework.core.io.ClassPathResource;
import org.springframework.r2dbc.connection.init.ResourceDatabasePopulator;

public class InputTypePopulator extends ResourceDatabasePopulator {

  public static final String CAMT_CSV_READER = "CAMT CSV Reader",
      SPARKASSE = CAMT_CSV_READER;

  public static final String CAMT_CSV_READER_BEAN = "camtCsvReader",
      SPARKASSE_BEAN = CAMT_CSV_READER_BEAN;

  public InputTypePopulator() {
    super(new ClassPathResource("db/migration/V1_populate_input_types.sql"));
  }
  
}
