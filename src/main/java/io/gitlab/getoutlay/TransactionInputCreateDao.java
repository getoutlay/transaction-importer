package io.gitlab.getoutlay;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;
import javax.validation.constraints.NotNull;


public class TransactionInputCreateDao {

  @JsonProperty
  @NotNull
  private String content;

  @JsonProperty
  @NotNull
  private long accountId;

  public String getContent() {
    return content;
  }


  public void setContent(String content) {
    this.content = content;
  }

  public long getAccountId() {
    return accountId;
  }

  public void setAccountId(long accountId) {
    this.accountId = accountId;
  }

}
