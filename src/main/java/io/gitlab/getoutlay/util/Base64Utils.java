package io.gitlab.getoutlay.util;

import java.nio.charset.StandardCharsets;
import javax.xml.bind.DatatypeConverter;

public class Base64Utils {

  static final String HEADER = "data:text/csv;base64,";

  public static String decodeBase64(String base64String) {
    if (base64String.startsWith(HEADER))
      base64String = base64String.substring(HEADER.length());
    return new String(DatatypeConverter.parseBase64Binary(base64String), StandardCharsets.UTF_8);
  }
}
