package io.gitlab.getoutlay.reader;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CamtDateDeserializer extends StdDeserializer<Date> {

  static final SimpleDateFormat FORMAT
      = new SimpleDateFormat("dd.MM.yyyy");

  protected CamtDateDeserializer() {
    super(Date.class);
  }

  @Override
  public Date deserialize(JsonParser parser, DeserializationContext ctxt) throws IOException {
    final String value = parser.getValueAsString();
    return parseValue(value);
  }

  private Date parseValue(String value) {
    if (value != null) {
      try {
        return FORMAT.parse(value);
      } catch (ParseException e) {
        return null;
      }
    } else {
      return null;
    }
  }
}
