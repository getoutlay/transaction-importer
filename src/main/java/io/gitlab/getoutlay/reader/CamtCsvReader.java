package io.gitlab.getoutlay.reader;

import static java.util.Spliterators.spliteratorUnknownSize;
import static java.util.stream.StreamSupport.stream;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvParser.Feature;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import io.gitlab.getoutlay.vault.client.Transaction;
import java.io.IOException;
import java.util.List;
import java.util.Spliterator;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class CamtCsvReader extends TransactionReader {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final ObjectReader reader;

  public CamtCsvReader() {
    CsvMapper mapper = new CsvMapper();
    CsvSchema schema = mapper.schemaFor(CamtTransactionEntry.class)
        .withHeader()
        .withColumnSeparator(';');
    mapper.configure(Feature.FAIL_ON_MISSING_COLUMNS, true);
    this.reader = mapper.readerFor(CamtTransactionEntry.class)
        .with(schema);
  }

  @Override
  protected List<Transaction> read(String content) {
    if (!StringUtils.hasText(content)) {
      return null;
    }

    try {
      MappingIterator<CamtTransactionEntry> iterator = reader.readValues(content);
      List<Transaction> result = stream(spliteratorUnknownSize(iterator, Spliterator.ORDERED), false)
          .map(t -> {
            Transaction transaction = new Transaction();
            transaction.setExplanation(t.bookingText);
            transaction.setDesription(t.description);
            transaction.setAmount(t.amount);
            transaction.setCurrency(t.currency);
            transaction.setBookedOn(t.bookingDay != null ? t.bookingDay.toInstant() : null);
            transaction.setExecutedOn(t.executionDay != null ? t.executionDay.toInstant() : null);
            transaction.setReference(t.reference);
            transaction.setCorresponderName(t.receiver);
            transaction.setCorresponderAccount(t.accountNumber);
            transaction.setCorresponderBank(t.swiftCode);
            return transaction;
          })
          .collect(Collectors.toList());
      log.info("{} entries read.", result.size());
      return result;
    } catch (IOException e) {
      log.warn("Cannot read content", e);
      return null;
    }
  }

}
