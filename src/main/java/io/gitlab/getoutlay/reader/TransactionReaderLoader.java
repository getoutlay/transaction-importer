package io.gitlab.getoutlay.reader;

import io.gitlab.getoutlay.InputType;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class TransactionReaderLoader {

  private final Logger logger = LoggerFactory.getLogger(getClass());

  @Autowired
  ApplicationContext applicationContext;

  public Optional<TransactionReader> loadReaderFor(InputType inputType) {
    logger.info("Loading reader for {} ...", inputType);

    if (inputType == null) {
      return Optional.empty();
    }

    String readerBeanName = inputType.getImporter();
    logger.info("Loading reader with type {} ...", readerBeanName);
    if (readerBeanName == null || "".equals(readerBeanName)) {
      return Optional.empty();
    }

    try {
      TransactionReader transactionReader = (TransactionReader) applicationContext.getBean(readerBeanName);
      return  Optional.of(transactionReader);
    } catch (ClassCastException | BeansException e) {
      logger.warn("Could not create Bean for " + readerBeanName, e);
      return Optional.empty();
    }
  }
}
