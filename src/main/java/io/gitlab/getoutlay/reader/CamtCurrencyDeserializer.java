package io.gitlab.getoutlay.reader;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import io.gitlab.getoutlay.vault.client.Currency;
import java.io.IOException;

public class CamtCurrencyDeserializer extends StdDeserializer<Currency> {

  protected CamtCurrencyDeserializer() {
    super(Currency.class);
  }

  @Override
  public Currency deserialize(JsonParser parser, DeserializationContext ctxt) throws IOException {
    final String value = parser.getValueAsString();
    return parseValue(value);
  }

  private Currency parseValue(String value) {
    try {
      return Currency.valueOf(value);
    } catch (IllegalArgumentException e) {
      return null;
    }
  }
}
