package io.gitlab.getoutlay.reader;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import io.gitlab.getoutlay.vault.client.Currency;
import java.util.Date;

public class CamtTransactionEntry {

  @JsonProperty(required = true, index = 0, value = "Auftragskonto")
  String accountCode;

  @JsonDeserialize(using = CamtDateDeserializer.class)
  @JsonProperty(required = true, index = 1, value = "Buchungstag")
  Date bookingDay;

  @JsonDeserialize(using = CamtDateDeserializer.class)
  @JsonProperty(required = true, index = 2, value = "Valutadatum")
  Date executionDay;

  @JsonProperty(required = true, index = 3, value = "Buchungstext")
  String bookingText;

  @JsonProperty(required = true, index = 4, value = "Verwendungszweck")
  String description;

  @JsonProperty(required = true, index = 5, value = "Glaeubiger ID")
  String creditorId;

  @JsonProperty(required = true, index = 6, value = "Mandatsreferenz")
  String reference;

  @JsonProperty(required = true, index = 7, value = "Kundenreferenz (End-to-End)")
  String customerReference;

  @JsonProperty(required = true, index = 8, value = "Sammlerreferenz")
  String collectorReference;

  @JsonDeserialize(using = CamtAmountDeserializer.class)
  @JsonProperty(required = true, index = 9, value = "Lastschrift Ursprungsbetrag")
  Double originalAmount;

  @JsonProperty(required = true, index = 10, value = "Auslagenersatz Ruecklastschrift")
  String returnDebitNote;

  @JsonProperty(required = true, index = 11, value = "Beguenstigter/Zahlungspflichtiger")
  String receiver;

  @JsonProperty(required = true, index = 12, value = "Kontonummer/IBAN")
  String accountNumber;

  @JsonProperty(required = true, index = 13, value = "BIC (SWIFT-Code)")
  String swiftCode;

  @JsonDeserialize(using = CamtAmountDeserializer.class)
  @JsonProperty(required = true, index = 14, value = "Betrag")
  Double amount;

  @JsonDeserialize(using = CamtCurrencyDeserializer.class)
  @JsonProperty(required = true, index = 15, value = "Waehrung")
  Currency currency;

  @JsonProperty(required = true, index = 16, value = "Info")
  String info;

  public String getAccountCode() {
    return accountCode;
  }

  public void setAccountCode(String accountCode) {
    this.accountCode = accountCode;
  }

  public Date getBookingDay() {
    return bookingDay;
  }

  public void setBookingDay(Date bookingDay) {
    this.bookingDay = bookingDay;
  }

  public Date getExecutionDay() {
    return executionDay;
  }

  public void setExecutionDay(Date executionDay) {
    this.executionDay = executionDay;
  }

  public String getBookingText() {
    return bookingText;
  }

  public void setBookingText(String bookingText) {
    this.bookingText = bookingText;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getCreditorId() {
    return creditorId;
  }

  public void setCreditorId(String creditorId) {
    this.creditorId = creditorId;
  }

  public String getReference() {
    return reference;
  }

  public void setReference(String reference) {
    this.reference = reference;
  }

  public String getCustomerReference() {
    return customerReference;
  }

  public void setCustomerReference(String customerReference) {
    this.customerReference = customerReference;
  }

  public String getCollectorReference() {
    return collectorReference;
  }

  public void setCollectorReference(String collectorReference) {
    this.collectorReference = collectorReference;
  }

  public Double getOriginalAmount() {
    return originalAmount;
  }

  public void setOriginalAmount(Double originalAmount) {
    this.originalAmount = originalAmount;
  }

  public String getReturnDebitNote() {
    return returnDebitNote;
  }

  public void setReturnDebitNote(String returnDebitNote) {
    this.returnDebitNote = returnDebitNote;
  }

  public String getReceiver() {
    return receiver;
  }

  public void setReceiver(String receiver) {
    this.receiver = receiver;
  }

  public String getAccountNumber() {
    return accountNumber;
  }

  public void setAccountNumber(String accountNumber) {
    this.accountNumber = accountNumber;
  }

  public String getSwiftCode() {
    return swiftCode;
  }

  public void setSwiftCode(String swiftCode) {
    this.swiftCode = swiftCode;
  }

  public Double getAmount() {
    return amount;
  }

  public void setAmount(Double amount) {
    this.amount = amount;
  }

  public Currency getCurrency() {
    return currency;
  }

  public void setCurrency(Currency currency) {
    this.currency = currency;
  }

  public String getInfo() {
    return info;
  }

  public void setInfo(String info) {
    this.info = info;
  }
}
