package io.gitlab.getoutlay.reader;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

public class CamtAmountDeserializer extends StdDeserializer<Double> {

  private static final NumberFormat FORMAT = new DecimalFormat("###,##", DecimalFormatSymbols.getInstance(Locale.GERMANY));


  protected CamtAmountDeserializer() {
    super(Double.class);
  }

  @Override
  public Double deserialize(JsonParser parser, DeserializationContext ctxt) throws IOException {
    final String value = parser.getValueAsString();
    return parseValue(value);
  }

  private Double parseValue(String value) {
    if (value != null) {
      try {
        return FORMAT.parse(value).doubleValue();
      } catch (ParseException e) {
        return null;
      }
    } else {
      return null;
    }
  }
}
