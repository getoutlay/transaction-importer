package io.gitlab.getoutlay.reader;

import io.gitlab.getoutlay.TransactionInput;
import io.gitlab.getoutlay.vault.client.Transaction;
import java.util.List;

public abstract class TransactionReader {

  public List<Transaction> read(TransactionInput input) {
    if (input == null || input.getContent() == null) {
      return null;
    }
    return read(input.getContent());
  }

  protected abstract List<Transaction> read(String content);

}
