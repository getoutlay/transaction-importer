package io.gitlab.getoutlay;

import static io.gitlab.getoutlay.util.FluxUtils.wrapNotFoundIfEmpty;

import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/input-types")
public class InputTypeController {

  private final Logger logger = LoggerFactory.getLogger(getClass());
  private final InputTypeRepository repository;
  

  public InputTypeController(InputTypeRepository repository) {
    this.repository = repository;
    
  }

  @GetMapping()
  public Flux<InputType> listAll() {
    logger.debug("listAll()");
    return repository.findAll();
  }

  @GetMapping("/{id}")
  public Mono<InputType> getOne(@PathVariable Long id) {
    logger.debug("getOne({})", id);
    return wrapNotFoundIfEmpty(repository.findById(id), "inputType", id);
  }

  @DeleteMapping("/{id}")
  public Mono<InputType> delete(@PathVariable Long id) {
    logger.debug("delete({})", id);
    return wrapNotFoundIfEmpty(repository.findById(id)
        .flatMap(inputType -> repository.delete(inputType).thenReturn(inputType)), "inputType", id);
  }

  @PostMapping()
  @ResponseStatus(HttpStatus.CREATED)
  public Mono<InputType> create(@RequestBody @Valid InputType inputType) {
    logger.debug("create({})", inputType);
    return repository.save(inputType);
  }

  @PutMapping("/{id}")
  public Mono<InputType> update(@PathVariable Long id, @RequestBody @Valid InputType entity) {
    logger.debug("create({} : {})", id, entity);
    entity.setId(id);
    return wrapNotFoundIfEmpty(repository.save(entity), "inputType", id);
  }
  
  
}
