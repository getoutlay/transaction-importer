package io.gitlab.getoutlay.batch;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ItemProcessListener;
import org.springframework.batch.core.ItemReadListener;
import org.springframework.batch.core.ItemWriteListener;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.annotation.AfterJob;
import org.springframework.batch.core.annotation.AfterProcess;
import org.springframework.batch.core.annotation.AfterRead;
import org.springframework.batch.core.annotation.AfterWrite;
import org.springframework.batch.core.annotation.BeforeJob;
import org.springframework.batch.core.annotation.BeforeProcess;
import org.springframework.batch.core.annotation.BeforeRead;
import org.springframework.batch.core.annotation.OnProcessError;
import org.springframework.batch.core.annotation.OnReadError;
import org.springframework.batch.core.annotation.OnWriteError;

public class BatchLogAdapter<I, O> implements JobExecutionListener, ItemReadListener<I>, ItemProcessListener<I, O>, ItemWriteListener<O> {

  final Logger logger;

  public BatchLogAdapter() {
    this(LoggerFactory.getLogger(BatchLogAdapter.class.getPackageName()));
  }

  public BatchLogAdapter(Logger logger) {
    this.logger = logger;
  }

  @Override
  @BeforeJob
  public void beforeJob(JobExecution jobExecution) {
    logger.info("Before Job: {}", jobExecution);
  }

  @Override
  @AfterJob
  public void afterJob(JobExecution jobExecution) {
    logger.info("After Job: {}", jobExecution);
  }

  @Override
  @BeforeRead
  public void beforeRead() {
    logger.info("beforeRead");
  }

  @Override
  @AfterRead
  public void afterRead(I item) {
    logger.info("afterRead: {}", item);
  }

  @Override
  @OnReadError
  public void onReadError(Exception ex) {
    logger.error("Read Error", ex);
  }

  @Override
  @BeforeProcess
  public void beforeProcess(I item) {
    logger.info("beforeProcess: {}", item);
  }

  @Override
  @AfterProcess
  public void afterProcess(I item, O result) {
    logger.info("afterProcess: {}", result);
  }

  @Override
  @OnProcessError
  public void onProcessError(I item, Exception e) {
    logger.error("onProcessError", e);
  }

  @Override
  public void beforeWrite(List<? extends O> items) {
    logger.info("beforeProcess: {}", items);
  }

  @Override
  @AfterWrite
  public void afterWrite(List<? extends O> items) {
    logger.info("beforeProcess: {}", items);
  }

  @Override
  @OnWriteError
  public void onWriteError(Exception ex, List<? extends O> items) {
    logger.error("onWriteError", ex);
  }

}
