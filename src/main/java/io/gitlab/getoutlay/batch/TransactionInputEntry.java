package io.gitlab.getoutlay.batch;

import io.gitlab.getoutlay.TransactionInput;
import io.gitlab.getoutlay.vault.client.Transaction;
import java.util.List;

public class TransactionInputEntry {

  private final TransactionInput transactionInput;
  private final List<Transaction> transactions;

  public TransactionInputEntry(TransactionInput transactionInput, List<Transaction> transactions) {
    this.transactionInput = transactionInput;
    this.transactions = transactions;
  }

  public TransactionInput getTransactionInput() {
    return transactionInput;
  }

  public List<Transaction> getTransactions() {
    return transactions;
  }
}
