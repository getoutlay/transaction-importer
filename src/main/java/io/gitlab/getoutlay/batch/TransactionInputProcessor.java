package io.gitlab.getoutlay.batch;

import io.gitlab.getoutlay.InputStatus;
import io.gitlab.getoutlay.InputTypeRepository;
import io.gitlab.getoutlay.TransactionInput;
import io.gitlab.getoutlay.TransactionInputRepository;
import io.gitlab.getoutlay.reader.TransactionReader;
import io.gitlab.getoutlay.reader.TransactionReaderLoader;
import io.gitlab.getoutlay.vault.client.Transaction;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

@Service
public class TransactionInputProcessor implements ItemProcessor<TransactionInput, TransactionInputEntry> {

  private final Logger logger = LoggerFactory.getLogger(getClass());

  @Autowired
  TransactionInputRepository transactionInputRepository;

  @Autowired
  InputTypeRepository inputTypeRepository;

  @Autowired
  TransactionReaderLoader transactionReaderLoader;

  @Override
  public TransactionInputEntry process(TransactionInput transactionInput) {
    Assert.notNull(transactionInput, "TransactionInput cannot be null");
    logger.debug("Processing TransactionInput: {}", transactionInput);
    logger.info("Processing TransactionInput with ID: {}", transactionInput.getId());

    Long inputTypeId = transactionInput.getTypeId();
    transactionInput.setStatus(InputStatus.RUNNING);

    Optional<TransactionReader> reader = transactionInputRepository.save(transactionInput)
        .blockOptional()
        .flatMap(ti -> inputTypeRepository.findById(inputTypeId).blockOptional())
        .map(inputType -> {
          logger.debug("Input Type: {}", inputType);
          logger.info("Input Type ID: {}", inputTypeId);
          return inputType;
        })
        .flatMap(inputType -> transactionReaderLoader.loadReaderFor(inputType))
        .map(imp -> {
          logger.debug("Importer: {}", imp);
          return imp;
        });

    if (reader.isPresent()) {
      logger.info("Importer found. Processing Input Type with ID: {}.", inputTypeId);
      Long accountId = transactionInput.getAccountId();
      List<Transaction> transactions = reader.stream()
          .map(transactionReader -> transactionReader.read(transactionInput))
          .flatMap(Collection::stream)
          .peek(transaction -> {
            transaction.setAccountId(accountId);
          })
          .collect(Collectors.toList());
      logger.info("{} Transactions found in Input Type with ID: {}.", transactions.size(), inputTypeId);
      return new TransactionInputEntry(transactionInput, transactions);
    } else {
      logger.warn("Reader cannot be loaded for Input Type with ID: {}. Skip Processing", inputTypeId);
      transactionInput.setStatus(InputStatus.FAILED);
      transactionInputRepository.save(transactionInput)
          .block();
      return null;
    }
  }
}
