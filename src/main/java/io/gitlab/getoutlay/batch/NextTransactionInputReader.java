package io.gitlab.getoutlay.batch;

import io.gitlab.getoutlay.InputStatus;
import io.gitlab.getoutlay.TransactionInput;
import io.gitlab.getoutlay.TransactionInputRepository;
import org.springframework.batch.item.ItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

@Service
public class NextTransactionInputReader implements ItemReader<TransactionInput> {

  @Autowired
  TransactionInputRepository transactionInputRepository;

  @Override
  public TransactionInput read() {
    InputStatus inputStatus = InputStatus.INITIALIZED;
    Sort sort = Sort.by("id").ascending();
    Flux<TransactionInput> result = transactionInputRepository.findByStatus(inputStatus, sort);
    return result.blockFirst();
  }
}
