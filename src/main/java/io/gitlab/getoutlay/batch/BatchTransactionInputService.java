package io.gitlab.getoutlay.batch;

import io.gitlab.getoutlay.TransactionInput;
import java.util.Collections;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BatchTransactionInputService {

  public static final String TRANSACTION_INPUT_ID = "input.id";

  @Autowired
  JobLauncher jobLauncher;

  @Autowired
  Job job;

  public void importTransactions(TransactionInput transactionInput) {
    JobParameters parameters = parameters(transactionInput.getId());
    try {
      jobLauncher.run(job, parameters);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private JobParameters parameters(Long transactionInputId) {
    return new JobParameters(Collections.singletonMap(TRANSACTION_INPUT_ID, new JobParameter(transactionInputId)));
  }
  
}
