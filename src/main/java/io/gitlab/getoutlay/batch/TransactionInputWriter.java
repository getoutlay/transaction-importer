package io.gitlab.getoutlay.batch;

import io.gitlab.getoutlay.InputStatus;
import io.gitlab.getoutlay.TransactionInput;
import io.gitlab.getoutlay.TransactionInputRepository;
import io.gitlab.getoutlay.vault.client.RestClient;
import io.gitlab.getoutlay.vault.client.Transaction;
import io.gitlab.getoutlay.vault.client.VaultClient;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import reactor.core.publisher.Flux;

@Service
public class TransactionInputWriter implements ItemWriter<TransactionInputEntry> {

  private final Logger logger = LoggerFactory.getLogger(getClass());

  @Autowired
  TransactionInputRepository transactionInputRepository;

  @Autowired
  VaultClient vaultClient;

  @Override
  public void write(List<? extends TransactionInputEntry> items) {
    Assert.notNull(items, "Entries cannot be null");
    Assert.noNullElements(items, "Entry elements cannot be null");
    Assert.notNull(vaultClient, "VaultClient cannot be null");
    Assert.notNull(transactionInputRepository, "TransactionInputRepository cannot be null");

    final RestClient<Transaction> transactionClient = vaultClient.transaction();
    items.forEach(entry -> {
      final TransactionInput transactionInput = entry.getTransactionInput();
      Assert.notNull(entry.getTransactionInput(), "TransactionInput cannot be null for entry: " + entry);

      if (entry.getTransactions() == null || entry.getTransactions().isEmpty()) {
        transactionInput.setStatus(InputStatus.SUCCESS);
        transactionInputRepository.save(transactionInput)
            .block();
      } else {
        try {
          TransactionInput input = Flux.fromStream(entry.getTransactions()
              .stream())
              .flatMap(transactionClient::create)
              .collectList()
              .flatMap(transactions -> {
                transactionInput.setStatus(InputStatus.SUCCESS);
                return transactionInputRepository.save(transactionInput);
              })
              .block();
          logger.info("Completing writing entries for input: {}", input);
        } catch (Exception e) {
          logger.error("Exception occurred writing transactions, skipping to the next item.", e);
          transactionInput.setStatus(InputStatus.FAILED);
          transactionInputRepository.save(transactionInput)
              .block();
        }
      }
    });
  }
}
