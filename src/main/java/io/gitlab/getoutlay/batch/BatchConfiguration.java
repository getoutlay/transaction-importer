package io.gitlab.getoutlay.batch;

import io.gitlab.getoutlay.TransactionInput;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.BatchConfigurer;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.batch.BatchProperties;
import org.springframework.boot.autoconfigure.batch.JpaBatchConfigurer;
import org.springframework.boot.autoconfigure.transaction.TransactionManagerCustomizers;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;

@Configuration
@ConfigurationProperties
@EnableBatchProcessing
@EnableAutoConfiguration
public class BatchConfiguration {

  @Autowired
  JobBuilderFactory jobs;

  @Autowired
  StepBuilderFactory steps;

  @Autowired
  NextTransactionInputReader nextTransactionInputReader;

  @Autowired
  TransactionInputProcessor transactionInputProcessor;

  @Autowired
  TransactionInputWriter transactionInputWriter;

  @Bean
  public BatchLogAdapter<TransactionInput, TransactionInputEntry> logger() {
    return new BatchLogAdapter<>();
  }

  @Bean
  public TaskExecutor taskExecutor() {
    return new SimpleAsyncTaskExecutor();
  }

  @Bean
  public JobLauncher simpleJobLauncher(JobRepository jobRepository) throws Exception {
    SimpleJobLauncher jobLauncher = new SimpleJobLauncher();
    jobLauncher.setJobRepository(jobRepository);
    jobLauncher.setTaskExecutor(taskExecutor());
    jobLauncher.afterPropertiesSet();
    return jobLauncher;
  }

  @Bean
  public BatchConfigurer batchConfigurer(BatchProperties properties, DataSource dataSource,
      TransactionManagerCustomizers transactionManagerCustomizers, EntityManagerFactory entityManagerFactory,
      JobRepository jobRepository) {
    return new JpaBatchConfigurer(properties, dataSource, transactionManagerCustomizers, entityManagerFactory) {
      @Override
      protected JobLauncher createJobLauncher() throws Exception {
        return simpleJobLauncher(jobRepository);
      }
    };
  }

  @Bean
  public Job importTransactionJob(Step readTransactionInput, BatchLogAdapter<TransactionInput, TransactionInputEntry> logger) {
    return this.jobs.get("importTransactionJob")
        .listener(logger)
        .start(readTransactionInput)
        .build();
  }

  @Bean
  public Step readTransactionInput(BatchLogAdapter<TransactionInput, TransactionInputEntry> logger) {
    return steps.get("readTransactionInput")
        .<TransactionInput, TransactionInputEntry>chunk(1)
        .reader(nextTransactionInputReader)
        .processor(transactionInputProcessor)
        .writer(transactionInputWriter)
        .listener((Object) logger)
        .build();
  }

}
