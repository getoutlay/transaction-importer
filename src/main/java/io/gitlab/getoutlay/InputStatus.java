package io.gitlab.getoutlay;

public enum InputStatus {

  INITIALIZED,
  RUNNING,
  SUCCESS,
  FAILED,
  ;
}
