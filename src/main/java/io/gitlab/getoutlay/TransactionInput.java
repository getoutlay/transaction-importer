package io.gitlab.getoutlay;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;
import javax.validation.constraints.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;


@Table("T_TRANSACTION_INPUT")
public class TransactionInput {

  @Id
  private Long id;
  
  @NotNull
  @Column("CONTENT")
  @JsonProperty
  private String content;

  @NotNull
  @Column("TYPE_ID")
  @JsonProperty
  private Long typeId;

  @NotNull
  @Column("STATUS")
  @JsonProperty
  private InputStatus status;

  @NotNull
  @Column("ACCOUNT_ID")
  @JsonProperty
  private Long accountId;

  public Long getId() {
    return id;
  }

  
  public String getContent() { return content; }

  public Long getTypeId() { return typeId; }

  public InputStatus getStatus() { return status; }

  public Long getAccountId() { return accountId; }

  public void setId(Long id) {
    this.id = id;
  }

  
  public void setContent(String content) { this.content = content; }

  public void setTypeId(Long typeId) { this.typeId = typeId; }

  public void setStatus(InputStatus status) { this.status = status; }

  public void setAccountId(Long accountId) { this.accountId = accountId; }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TransactionInput transactionInput = (TransactionInput) o;
    return Objects.equals(id, transactionInput.id)
        && Objects.equals(content, transactionInput.content)
        && Objects.equals(typeId, transactionInput.typeId)
        && Objects.equals(status, transactionInput.status)
        && Objects.equals(accountId, transactionInput.accountId);
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("TransactionInput{");
    sb.append("id=").append(id);
    sb.append(", content=").append(content);
    sb.append(", typeId=").append(typeId);
    sb.append(", status=").append(status);
    sb.append(", accountId=").append(accountId);
    sb.append('}');
    return sb.toString();
  }

}
