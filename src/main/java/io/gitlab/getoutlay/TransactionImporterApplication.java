package io.gitlab.getoutlay;

import io.gitlab.getoutlay.batch.BatchConfiguration;
import io.gitlab.getoutlay.batch.BatchDataSourceConfiguration;
import io.gitlab.getoutlay.vault.client.VaultClient;
import io.gitlab.getoutlay.vault.client.VaultClientConfiguration;
import io.gitlab.getoutlay.vault.client.VaultClientConfigurationProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication(proxyBeanMethods = false)
@Configuration
@EnableConfigurationProperties({io.gitlab.getoutlay.db.DatabaseMigrationProperties.class,
     BatchConfiguration.class, BatchDataSourceConfiguration.class, VaultClientConfigurationProperties.class})
//mastic-pin-annotation
public class TransactionImporterApplication {

  @Autowired
  private VaultClientConfiguration vaultClientConfiguration;

  public TransactionImporterApplication() {
  }

  @Bean
  public VaultClient vaultClient(){
    return new VaultClient(vaultClientConfiguration);
  }

  public static void main(String[] args) {
    SpringApplication.run(TransactionImporterApplication.class, args);
  }

}
