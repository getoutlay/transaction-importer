package io.gitlab.getoutlay;

import org.springframework.data.domain.Sort;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface TransactionInputRepository extends ReactiveCrudRepository<TransactionInput, Long> {
  Flux<TransactionInput> findByStatus(InputStatus status, Sort id);
}
